import React from 'react';
import './App.css';

import Container from 'react-bootstrap/Container';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Navigation from './navigation';
import ExerciseCard from './exercise-card';

const api = {
    path: "/api",
    endpoints: {
        openCloze: "/opencloze",
        nextOpenCloze: "/next"
    },
    getOpenClozeUrl: function () {
        return this.path + this.endpoints.openCloze;
    },
    getNextOpenClozeUrl: function () {
        return (this.getOpenClozeUrl() + this.endpoints.nextOpenCloze);
    }

};
const linkedinUrl = "https://www.linkedin.com/in/acruzg/?locale=en_US";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            score: {
                ok: 0,
                fail: 0
            }
        };

        //bindings
        this.scoreHandler = this.scoreHandler.bind(this);
        this.clearScore = this.clearScore.bind(this);
    }

    initScore() {
        if (!localStorage.score) {
            localStorage.setItem("score", true);
            localStorage.setItem("ok", 0);
            localStorage.setItem("fail", 0);
        }
        this.setState({
            score: {
                ok: Number(localStorage.getItem("ok")),
                fail: Number(localStorage.getItem("fail"))
            }
        });
    }

    clearScore () {
        this.setState({
            score: {
                ok: 0,
                fail: 0
            }
        });
        localStorage.clear();
    }

    scoreHandler = (correct, failed) =>{
        const {ok, fail} = this.state.score;
        this.setState({
            score: {
                ok: ok + correct,
                fail: fail + failed
            }
        });
    }

    // LIFECYCLE
    componentDidMount() {
        this.initScore();
    }

    componentDidUpdate() {
        if (localStorage.score) {
            const {ok, fail} = this.state.score;
            localStorage.setItem("ok", Number(ok));
            localStorage.setItem("fail", Number(fail));
        } else {
            this.initScore();
        }

    }

    render() {
        const { score } = this.state;
        return (
            <div className="App">
                <Navigation
                    className="navigation-component"
                    score={score}
                    clearScore={this.clearScore}
                ></Navigation>
                <Container>
                    <Row>
                        <Col md="8">
                            <ExerciseCard
                                title="Open Cloze"
                                api={{
                                    base: api.getOpenClozeUrl(),
                                    next: api.getNextOpenClozeUrl()
                                }}
                                scoreHandler = {this.scoreHandler}
                            />
                            <p className="creator-label text-uppercase">
                                <small className="text-secondary">
                                    Project |
                                    <code
                                        onClick={() => {
                                            window.open(linkedinUrl, "_blank");
                                        }}
                                    >
                                        Andres Cruz Garcia
                                    </code>
                                </small>
                            </p>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default App;