import React from "react";
import Axios from "axios";
import PropTypes from 'prop-types';

import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";

import CardText from './CardText';
import Loader from './Loader';
import AnswerModal from './AnswerModal';

//unique identifiers, random based
const uuid = require("uuid/v4");

const propTypes = {
    title: PropTypes.string,
    api: PropTypes.exact({
        base: PropTypes.string.isRequired,
        next: PropTypes.string.isRequired
    }),
    scoreHandler: PropTypes.func.isRequired
};

const defaultProps = {
    title: "Exercise"
};

const MESSAGE_ERROR_SUBMIT = "Uh-oh! There was a problem getting the results back. Please try again.";

class ExerciseCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetching: true,
            error: false,
            errorMessage: undefined,
            answered: false,
            displayErrors: false,
            answer: {
                inputs: {} //inputs are stored & referenced by name property (as key)
            },
            exercise: {},
            showModal: false
        };

        //bindings
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        //local member for accessing parent function
        this.scoreHandler = props.scoreHandler;
    }

    // HANDLERS
    handleSubmit(event) {
        event.preventDefault();
        if (this.state.answered) {
            // if already answered, reset and fetch next exercise
            this.setState({
                answered: false
            });
            // until better feature implementation in backend
            //this.loadNextExercise();
            this.loadRandomExercise();
        } else {
            // try to submit answer
            if (!event.target.checkValidity()) {
                // if there are any errors
                this.setState({ displayErrors: true });
                return;
            }

            this.setState({ fetching: true });

            const payload = {};
            let words = [];

            Object.entries(this.state.answer.inputs).forEach(input => {
                const [, value] = input; //skip the 'key' of the [key,value] pair
                words.push(value.trim());
            });

            payload.exerciseKey = this.state.exercise.key;
            payload.words = words;

            Axios.post(this.props.api.base, payload)
                .then(res => {
                    const exerciseTmp = { ...this.state.exercise };
                    exerciseTmp.answer = res.data;
                    // proxy to update score in parent state
                    this.updateScore(res.data);
                    this.setState({
                        exercise: exerciseTmp,
                        fetching: false,
                        displayErrors: false,
                        answered: true,
                        answer: {
                            inputs: {}
                        }
                    });
                })
                .catch(() => {
                    this.setState({
                        fetching: false,
                        error: true,
                        errorMessage: MESSAGE_ERROR_SUBMIT,
                        answer: {
                            inputs: {}
                        }
                    });
                });
        }
    }
    handleInputChange(event) {
        const input = event.target;
        const tmpInputs = { ...this.state.answer.inputs };
        tmpInputs[input.name] = input.value;
        this.setState({
            answer: {
                inputs: tmpInputs
            }
        });
        return;
    }
    handleShowModal() {
        this.setState({ showModal: true });
    }
    handleCloseModal() {
        this.setState({ showModal: false });
    }

    // UTILS
    updateScore(answers) {
        let ok, failed = 0;
        ok = answers.reduce((acc, answer) => {
            return answer.correct ? acc + 1 : acc;
        }, 0);
        failed = answers.length - ok;
        this.scoreHandler(ok, failed); // from parent
    }
    // LOADERS
    loadNextExercise() {
        this.setState({ fetching: true, error: false });
        Axios.get(this.props.api.next, {
            params: {
                key: this.state.exercise.key
            }
        })
            .then(res => this.setExercise(res))
            .catch(() => {
                this.loadRandomExercise();
            });
    }
    loadRandomExercise() {
        this.setState({ fetching: true, error: false });
        Axios.get(this.props.api.base)
            .then(res => this.setExercise(res))
            .catch(error => {
                this.setState({
                    fetching: false,
                    error: true,
                    exercise: {}
                });
                if (error.response) {
                    this.setState({
                        // if undefined, default props will kick in further down.
                        errorMessage: error.response.data.message
                    });
                }
            });
    }
    setExercise(res) {
        const { exerciseKey, words, gapPositions } = res.data;
        this.setState({
            fetching: false,
            exercise: {
                key: exerciseKey,
                words: words,
                gapPositions: gapPositions
            }
        });
    }

    // LIFECYCLE
    componentDidMount() {
        this.loadRandomExercise();
    }
    render() {
        const { title } = this.props;
        const {
            displayErrors,
            fetching,
            error,
            errorMessage,
            answer,
            answered,
            exercise,
            showModal
        } = this.state;
        return (
            <Card className="exercise">
                <Form
                    autoComplete="off"
                    inline="true"
                    noValidate
                    className={displayErrors ? "displayErrors" : ""}
                    onSubmit={this.handleSubmit}
                >
                    <Card.Body>
                        <Card.Title>{title}</Card.Title>
                        {fetching ? (
                            <Loader />
                        ) : (
                            <CardText
                                exercise={exercise}
                                answer={answer}
                                error={error}
                                errorMessage={errorMessage}
                                answered={answered}
                                handleInputChange={this.handleInputChange}
                                handleShowModal={this.handleShowModal}
                                fallback={() => this.loadRandomExercise()}
                            />
                        )}
                    </Card.Body>
                </Form>
                {!error && answered ? ( // conditional Modal render
                    <AnswerModal
                        key={uuid()}
                        answer={exercise.answer}
                        showModal={showModal}
                        handleCloseModal={this.handleCloseModal}
                    />
                ) : (
                    undefined
                )}
            </Card>
        );
    }
}

ExerciseCard.propTypes = propTypes;
ExerciseCard.defaultProps = defaultProps;

export default ExerciseCard;
