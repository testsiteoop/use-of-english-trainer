import React from "react";
import PropTypes from "prop-types";

import Button from "react-bootstrap/Button";

const propTypes = {
    isError: PropTypes.bool.isRequired,
    fallback: PropTypes.func.isRequired,
    buttonText: PropTypes.string
};

const defaultProps = { buttonText: "Submit" };

function CardButton({ buttonText, isError, fallback }) {
    return (
        <Button
            variant="primary"
            type="submit"
            onClick={isError ? fallback : undefined}
        >
            {buttonText}
        </Button>
    );
}

CardButton.propTypes = propTypes;
CardButton.defaultProps = defaultProps;

export default CardButton;
