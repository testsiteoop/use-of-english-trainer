import React from "react";
import PropTypes from "prop-types";

const propTypes = {
    correct: PropTypes.bool.isRequired,
    expected: PropTypes.string.isRequired,
    word: PropTypes.string.isRequired,
    onClickHandler: PropTypes.func.isRequired
};

function WordAnswer({ correct, expected, word, onClickHandler }) {
    return (
        <span
            className={
                "result-word correct text-" + (correct ? "success" : "danger")
            }
            title={'The correct word is: "' + expected + '"'}
            onClick={onClickHandler}
        >
            <strong>{word}</strong>
        </span>
    );
}

WordAnswer.propTypes = propTypes;

export default WordAnswer;
