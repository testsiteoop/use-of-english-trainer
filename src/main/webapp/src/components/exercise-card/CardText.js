import React, { useState } from "react";
import PropTypes from "prop-types";

import Card from "react-bootstrap/Card";
import CardButton from "./CardButton";

import WordGap from "./WordGap";
import WordAnswer from "./WordAnswer";

const propTypes = {
    exercise: PropTypes.object,
    answer: PropTypes.object,
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    answered: PropTypes.bool,
    handleInputChange: PropTypes.func,
    handleShowModal: PropTypes.func,
    fallback: PropTypes.func.isRequired
};

const defaultProps = {
    content: {
        button: "Submit"
    },
    errorMessage: "There was a problem loading the exercise."
};
//unique identifiers, random based
const uuid = require("uuid/v4");
function CardText(props) {
    const PUNCTUATION_TOKENS = [".", ",", ";", "?", "!", "…"];
    const SEPARATOR = " ";

    // last changed input. Initially set to the first gap, so that its focused when first rendered.
    const [lastChanged, setLastChanged] = useState(
        props.error ? undefined : props.exercise.gapPositions[0]
    );

    const buildOpenCloze = () => {
        const { exercise, answer, handleInputChange } = props;

        let tempText = "";
        let questionElements = [];

        exercise.words.forEach((element, index) => {
            if (exercise.gapPositions.includes(index)) {
                tempText += " ";
                questionElements.push(tempText);
                questionElements.push(
                    <WordGap
                        key={uuid()}
                        index={index}
                        value={answer.inputs[index]}
                        // need to call change handler for setting state in parent component
                        // AND set in local state which input has changed most recently
                        handleInputChange={e => {
                            handleInputChange(e);
                            setLastChanged(Number(e.target.name));
                        }}
                        // If it is the last one to have changed, then signal focus
                        focusSet={lastChanged === index ? true : false}
                    />
                );
                tempText = "";
            } else {
                if (PUNCTUATION_TOKENS.includes(element)) {
                    tempText += element;
                } else {
                    tempText += SEPARATOR + element;
                }
            }
            if (index === exercise.words.length - 1) {
                questionElements.push(tempText);
            }
        });
        return questionElements;
    };
    const buildAnswer = () => {
        const { exercise, handleShowModal } = props;

        let tempText = "";
        const answerElements = [];
        let wordCount = 0;

        exercise.words.forEach((element, index) => {
            if (exercise.gapPositions.includes(index)) {
                tempText += " ";
                answerElements.push(tempText);
                answerElements.push(
                    <WordAnswer
                        key={uuid()}
                        correct={exercise.answer[wordCount].correct}
                        expected={exercise.answer[wordCount].expected}
                        word={exercise.answer[wordCount].given}
                        onClickHandler={handleShowModal}
                    />
                );
                tempText = "";
                wordCount++;
            } else {
                if (PUNCTUATION_TOKENS.includes(element)) {
                    tempText += element;
                } else {
                    tempText += SEPARATOR + element;
                }
            }
            if (index === exercise.words.length - 1) {
                answerElements.push(tempText);
            }
        });

        return answerElements;
    };
    const getCardContent = () => {
        const text = {
            question: {
                button: "Check your answer!",
                subtitle: (
                    <>
                        Read the text below and think of the word which best
                        fits each gap. Use only <strong>one</strong> word in
                        each gap.
                    </>
                )
            },
            answer: {
                button: "Next Exercise",
                subtitle: <>Here are your results!</>
            },
            error: {
                button: "Try again",
                subtitle: <>:( Sorry...</>
            }
        };

        const { error, answered, errorMessage } = props;

        const status = error ? "error" : answered ? "answer" : "question";
        const { button, subtitle } = text[status];

        return {
            subtitle: subtitle,
            text: error
                ? errorMessage
                : answered
                ? buildAnswer()
                : buildOpenCloze(), // default, question
            button: button
        };
    };

    const content = getCardContent();
    const { error, fallback } = props;
    return (
        <>
            {content.subtitle}
            <Card.Text>{content.text}</Card.Text>
            <CardButton
                buttonText={content.button}
                isError={error}
                fallback={fallback}
            />
        </>
    );
}

CardText.propTypes = propTypes;
CardText.defaultProps = defaultProps;

export default CardText;
