import React, { useEffect } from "react";
import PropTypes from "prop-types";

import FormControl from "react-bootstrap/FormControl";

const propTypes = {
    value: PropTypes.string,
    index: PropTypes.number.isRequired,
    placeholder: PropTypes.string,
    handleInputChange: PropTypes.func.isRequired,
    focusSet: PropTypes.bool.isRequired
};

const defaultProps = {
    placeholder: "enter one word"
};

function WordGap(props) {
    const { value, index, placeholder, handleInputChange, focusSet} = props;
    const ref = React.createRef();
    // if mounted or updated, check if it needs to focus
    useEffect(()=>{
        if(focusSet){
            ref.current.focus();
        }
    });
    return (
        <FormControl
            ref={ref}
            value={value}
            name={index}
            placeholder={placeholder}
            onChange={handleInputChange}
            type="text"
            required
        ></FormControl>
    );
}

WordGap.propTypes = propTypes;
WordGap.defaultProps = defaultProps;

export default WordGap;
