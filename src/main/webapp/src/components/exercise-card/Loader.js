import React from "react";
import PropTypes from "prop-types";

import Spinner from "react-bootstrap/Spinner";

const propTypes = {message: PropTypes.string};
const defaultProps = {message: "Loading... Please wait a moment."};

function Loader({ message }) {
    return (
        <>
            <p>{message}</p>
            <Spinner animation="border" variant="primary" />
        </>
    );
}

Loader.propTypes = propTypes;
Loader.defaultProps = defaultProps;

export default Loader;
