import React from "react";
import PropTypes from "prop-types";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const propTypes = {
    showModal: PropTypes.bool.isRequired,
    handleCloseModal: PropTypes.func.isRequired,
    answer: PropTypes.arrayOf(
        PropTypes.shape({ expected: PropTypes.string.isRequired })
    )
};

function AnswerModal({ answer, showModal, handleCloseModal }) {
    //unique identifiers
    const uuidv1 = require("uuid/v1");

    const List = () => {
        let frags = [];
        for (let index = 0; index < answer.length; index++) {
            frags.push(
                <p key={uuidv1()} className="display-4">
                    <strong>{answer[index].expected}</strong>
                </p>
            );
        }
        return frags;
    };
    return (
        <Modal show={showModal} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>The correct answer is:</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <List />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleCloseModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

AnswerModal.propTypes = propTypes;

export default AnswerModal;
