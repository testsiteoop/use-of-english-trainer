import React from "react";
import PropTypes from "prop-types";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";

import Scoreboard from "./Scoreboard";

const propTypes = {
    score: PropTypes.shape({
        ok: PropTypes.number,
        fail: PropTypes.number
    }).isRequired,
    clearScore: PropTypes.func.isRequired,
};

const sourceCodeUrl = "https://bitbucket.org/testsiteoop/use-of-english-trainer/";
class Navigation extends React.Component {
    render() {
        const { score, clearScore} = this.props;
        return (
            <Navbar bg="primary" expand="lg" variant="dark">
                <Navbar.Brand>
                    <strong>English-Trainer</strong>
                </Navbar.Brand>
                <Scoreboard score={score} clearScore={clearScore} />
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {/* <Nav.Link active="true">
                            About
                        </Nav.Link> */}
                    </Nav>
                    <Button
                        onClick={() => {
                            window.open(sourceCodeUrl, "_blank");
                        }}
                        variant="info"
                    >
                        Check the source code!
                    </Button>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

Navigation.propTypes = propTypes;
export default Navigation;
