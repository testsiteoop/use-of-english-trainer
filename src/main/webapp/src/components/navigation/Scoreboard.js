import React from "react";
import PropTypes from "prop-types";

import Badge from "react-bootstrap/Badge";

import './Scoreboard.css';
import Button from "react-bootstrap/Button";

const propTypes = {
    score: PropTypes.shape({
        ok: PropTypes.number,
        fail: PropTypes.number
    }).isRequired,
    clearScore: PropTypes.func.isRequired
};

function Scoreboard ({score, clearScore}) {
    const {ok, fail} = score;
    return (
        <span className="scoreboard">
            <Badge variant="dark">
                <Badge className="score-number" variant="success">
                    {ok}
                </Badge>
                <Badge className="score-number" variant="danger">
                    {fail}
                </Badge>
            </Badge>
            <Button variant="light" size="sm" onClick={clearScore}>
                Clear Score
            </Button>
        </span>
    );
}

Scoreboard.propTypes = propTypes;
export default Scoreboard;
