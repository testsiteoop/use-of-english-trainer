package cl.crugar.englishtrainer.session.store;

import cl.crugar.englishtrainer.model.Exercise;

public interface ExerciseStorageHandler {
	public Exercise get(Integer key);
	public void put(Integer key, Exercise exercise);
}
