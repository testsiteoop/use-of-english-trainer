package cl.crugar.englishtrainer.session.store;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import cl.crugar.englishtrainer.model.Exercise;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

//TODO: expire exercises according to some policy
@Repository
@Getter
@Setter
@Accessors(chain = true)
public class ExerciseStore implements ExerciseStorageHandler {
	private Map<Integer, Exercise> store = new HashMap<>();

	@Override
	public Exercise get(Integer key) {
		if(store.containsKey(key)) {
			return store.get(key);
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Exercise not found");
		}
	}

	@Override
	public void put(Integer key, Exercise exercise) {
		store.put(key, exercise);
	}

}
