package cl.crugar.englishtrainer.service.nlp;

public interface NaturalLanguageProcessing {
	public String[] getTokens(String text);
	public String[] getTags(String[] tokens);
}
