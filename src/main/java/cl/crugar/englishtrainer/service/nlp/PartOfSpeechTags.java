package cl.crugar.englishtrainer.service.nlp;

/**
 * @author Andres Cruz
 * 
 * <p>Represents the parts of speech tag list according to the
 * 	<a href="https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html">Penn Treebank Project</a>
 * </p>
 * <p>The tags to be considered for blanks generation (open cloze) by the ExerciseGenerators have their
 * 	<b>{@code isOpenCloze}</b> field set to <b>{@code true}</b>.
 * </p>
 */
public enum PartOfSpeechTags {
//	ENUM	DESCRIPTION									IS_OPEN_CLOZE	
	CC		("Coordinating conjunction",					true	),
	CD		("Cardinal number",								false	),
	DT		("Determiner",									false	),
	EX		("Existential there",							true	),
	FW		("Foreign word",								false	),
	IN		("Preposition or subordinating conjunction",	true	),
	JJ		("Adjective",									false	),
	JJR		("Adjective, comparative",						false	),
	JJS		("Adjective, superlative",						false	),
	LS		("List item marker",							false	),
	MD		("Modal",										true	),
	NN		("Noun, singular or mass",						false	),
	NNS		("Noun, plural",								false	),
	NNP		("Proper noun, singular",						false	),
	NNPS	("Proper noun, plural",							false	),
	PDT		("Predeterminer",								true	),
	POS		("Possessive ending",							true	),
	PRP		("Personal pronoun",							false	),
	PRP$	("Possessive pronoun",							true	),
	RB		("Adverb",										false	),
	RBR		("Adverb, comparative",							false	),
	RBS		("Adverb, superlative",							false	),
	RP		("Particle",									true	),
	SYM		("Symbol",										false	),
	TO		("to",											false	),
	UH		("Interjection",								false	),
	VB		("Verb, base form",								false	),
	VBD		("Verb, past tense",							false	),
	VBG		("Verb, gerund or present participle",			false	),
	VBN		("Verb, past participle",						false	),
	VBP		("Verb, non-3rd person singular present",		false	),
	VBZ		("Verb, 3rd person singular present",			false	),
	WDT		("Wh-determiner",								true	),
	WP		("Wh-pronoun",									true	),
	WP$		("Possessive wh-pronoun",						true	),
	WRB		("Wh-adverb",									true	)
	;

	private final String description;
	private final boolean isOpenCloze;
	
	PartOfSpeechTags(String description, boolean isOpenCloze) {
		  this.description = description;
		  this.isOpenCloze = isOpenCloze;
		 }
	
	public String description() {
		return description;
	}
	
	public boolean isOpenCloze() {
		return isOpenCloze;
	}
	
	public static boolean isOpenCloze(String enumValue) {
		return valueOf(enumValue).isOpenCloze();
	}
}
