package cl.crugar.englishtrainer.service.nlp;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

@Service
@Slf4j
public class NLPService implements NaturalLanguageProcessing {
	
	@Override
	public String[] getTokens(String text) {
        InputStream inputStream = getClass().getResourceAsStream("/nlp/en-token.bin");
        TokenizerModel model;
		try {
			model = new TokenizerModel(inputStream);
		} catch (IOException e) {
			log.error("Error while reading NLP Tokenizer model", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in NLP tokens", e);
		}
        TokenizerME tokenizer = new TokenizerME(model);
        return tokenizer.tokenize(text);
	}

	@Override
	public String[] getTags(String[] tokens) {
	    InputStream inputStreamPOSTagger = getClass()
	  	      .getResourceAsStream("/nlp/en-pos-maxent.bin");
	  	    POSModel posModel;
			try {
				posModel = new POSModel(inputStreamPOSTagger);
			} catch (IOException e) {
				log.error("Error while reading NLP POS-TAGGER model", e);
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in NLP tags", e);
			}
	  	    POSTaggerME posTagger = new POSTaggerME(posModel);
	  	    return posTagger.tag(tokens);
	}

}
