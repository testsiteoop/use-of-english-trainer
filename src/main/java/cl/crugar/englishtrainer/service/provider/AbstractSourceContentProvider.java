package cl.crugar.englishtrainer.service.provider;

import cl.crugar.englishtrainer.service.manager.ResourceManagement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)

abstract class AbstractSourceContentProvider implements ContentProvider {
	ResourceManagement sourceManager;

}
