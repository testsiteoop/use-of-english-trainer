package cl.crugar.englishtrainer.service.provider;

import cl.crugar.englishtrainer.model.Content;

public interface ContentProvider {
    Content getRandom();
    Content getNext(Content content);
}
