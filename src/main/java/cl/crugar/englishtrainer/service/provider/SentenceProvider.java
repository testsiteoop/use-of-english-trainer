package cl.crugar.englishtrainer.service.provider;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import cl.crugar.englishtrainer.model.Content;
import cl.crugar.englishtrainer.service.manager.SentenceSourceManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SentenceProvider extends AbstractSourceContentProvider{
	
	private static final int MINIMUM_WORDS_IN_SENTENCE = 9;
	
	@Lazy
	@Autowired
	SentenceProvider(SentenceSourceManager sentenceSourceManager){
		this.sourceManager = sentenceSourceManager;
	}
	
	@Override
	public Content getRandom() {
		Content content = this.getContent();
		while(!hasEnoughWords(content.getText())) {
			content = this.getContent();
		}
		log.info("Current content id: {}", content.getIndex());
		return content;
	}

	@Override
	public Content getNext(Content content) {
		log.info("Current content id: {}", content.getIndex());
		// TODO: If an update of the source changes the number of elements, 
		// must make sure that the correct text is returned. 
		Content nextContent = this.getContent(content.getIndex() + 1);
		while(!hasEnoughWords(nextContent.getText())) {
			nextContent = this.getContent(nextContent.getIndex() + 1);
		}
		log.info("Next content id: {}", nextContent.getIndex());
		return nextContent;
	}
	
	private boolean hasEnoughWords(String text) {
		log.debug("Text length: {}. Minimum: {}", text.split(" ").length, MINIMUM_WORDS_IN_SENTENCE);
		return text.split(" ").length >= MINIMUM_WORDS_IN_SENTENCE;
	}
	
	private Content getContent() {
		List<String> texts = this.sourceManager.getTexts();
		int randomIndex = new Random().nextInt(texts.size());
		return new Content(randomIndex, texts.get(randomIndex));
	}
	
	private Content getContent(int nextIndex) {
		List<String> texts = this.sourceManager.getTexts();
		if (nextIndex > texts.size() - 1) {
			nextIndex = 0;
		}
		String text = texts.get(nextIndex);
		return new Content(nextIndex, text);
	}

}
