package cl.crugar.englishtrainer.service.result;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import cl.crugar.englishtrainer.model.Answer;
import cl.crugar.englishtrainer.model.Exercise;
import cl.crugar.englishtrainer.model.Result;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ResultChecker implements ResultAnalisis {

	@Override
	public List<Result> getResults(Answer answer, Exercise exercise) {
		List<Result> results = new ArrayList<>();
		
		List<String> givenAnswerWords = answer.getWords();
		List<String> expectedAnswerWords = exercise.getAnswer().getWords();
		
		if(givenAnswerWords.size() != expectedAnswerWords.size()) {
			log.error("number of words in received answer do not match exercise");
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected number of words in answer");
		}
		
		int i = 0;
		for (String givenWord : givenAnswerWords) {
			Result result = new Result();
			String expectedWord = expectedAnswerWords.get(i);
			result
				.setGiven(givenWord)
				.setExpected(expectedWord)
				.setCorrect(givenWord.equalsIgnoreCase(expectedWord)); //case INsensitive
			results.add(result);
			i++;
		}
		return results;
	}

}
