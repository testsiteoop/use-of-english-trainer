package cl.crugar.englishtrainer.service.result;

import java.util.List;

import cl.crugar.englishtrainer.model.Answer;
import cl.crugar.englishtrainer.model.Exercise;
import cl.crugar.englishtrainer.model.Result;

public interface ResultAnalisis {
	public List<Result> getResults(Answer answer, Exercise exercise);
}
