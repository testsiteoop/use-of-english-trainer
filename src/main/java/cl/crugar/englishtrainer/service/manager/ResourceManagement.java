package cl.crugar.englishtrainer.service.manager;

import java.net.URL;
import java.util.List;

public interface ResourceManagement {
	
	/**
	 * Loads the raw content from the given resource URL. 
	 * @param url
	 */
	void load(URL url);

	/**
	 * Refreshes the content from the source if the elapsed time since
	 * the last refresh is longer than what is configured. 
	 */
	void refresh();
	
	/**
	 * Returns the texts contained in the raw resource as a List. 
	 * @return
	 */
	List<String> getTexts();

}
