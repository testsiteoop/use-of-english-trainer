package cl.crugar.englishtrainer.service.manager;

import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
abstract class ContentSourceManager implements ResourceManagement{
	
	URL url;
	List<String> texts = Collections.synchronizedList(new ArrayList<>());
	Instant lastRefreshed;
	
}
