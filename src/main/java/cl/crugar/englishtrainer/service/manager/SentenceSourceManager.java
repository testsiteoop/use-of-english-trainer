package cl.crugar.englishtrainer.service.manager;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.crugar.englishtrainer.dto.SentenceResourceDto;
import lombok.extern.slf4j.Slf4j;

@Lazy
@Service
@Slf4j
public class SentenceSourceManager extends ContentSourceManager implements InitializingBean{
	
	private static final long REFRESH_THRESHOLD_SECONDS = 3600; // 1800 is 30 minutes
	
	@Value("${sentence-source-url}")
	private String sentenceSourceUrl;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.setUrl(new URL(sentenceSourceUrl));
		this.load(this.getUrl());		
	}	
	
	@Override
	public void load(URL url) {
		log.info("Loading sentences resource...");
		ObjectMapper mapper = new ObjectMapper();
		try {
			SentenceResourceDto resource = mapper.readValue(url, SentenceResourceDto.class);
			synchronized (this.texts) { //consistency while reloading data
				this.texts.clear();
				resource.getData().forEach(e -> {
					String sentence = e.getSentence();
					this.texts.add(sentence);
				});
			}
			log.info("There are {} sentences available.", this.texts.size());
		} catch (JsonParseException e) {
			log.error("Error while parsing JSON source", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Malformed JSON", e);
		} catch (JsonMappingException e) {
			log.error("Error while mapping", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error loading, sorry.", e);
		} catch (IOException e) {
			log.error("Error loading source", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error loading, sorry", e);
		}
		
		this.setLastRefreshed(Instant.now());
		log.info("Loading done. Last refresh: {}", this.lastRefreshed.toString());
		log.info("Refresh interval is set to {} seconds.", REFRESH_THRESHOLD_SECONDS);
	}

	@Override
	public void refresh() {
		if (Instant.now().getEpochSecond() - this.lastRefreshed.getEpochSecond() > REFRESH_THRESHOLD_SECONDS) {
			log.info("Refreshing sentences resource...");
			this.load(getUrl());
		}
		else {
			log.debug("Refresh not needed.");
		}
	}

	@Override
	public List<String> getTexts() {
		this.refresh();
		return this.texts;
	}
	

}
