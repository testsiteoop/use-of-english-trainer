package cl.crugar.englishtrainer.service.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import cl.crugar.englishtrainer.model.Content;
import cl.crugar.englishtrainer.model.Exercise;
import cl.crugar.englishtrainer.service.nlp.NaturalLanguageProcessing;
import cl.crugar.englishtrainer.service.nlp.PartOfSpeechTags;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OpenClozeGenerator implements ExerciseGenerator {
	
	private static final String OPEN_CLOZE_PLACEHOLDER = "___";
	private static final double GAP_QUESTION_RATIO = 0.12;
	private static final int GAP_BOUNDARY = 2;
	
	private static final String OPEN_CLOZE_GENERATION_ERROR_MESSAGE = "Sentence loaded is not suitable for this kind of exercise. Please try again.";
	
	@Autowired
	private NaturalLanguageProcessing nlpProcessor;
	
	@Override	
	public Exercise generate(Content content) {
		log.info("Generating exercise...");
		String text = content.getText();
		log.info("Sentence: {}", text);
		
		String[] tokens = nlpProcessor.getTokens(text);
		String[] tags = nlpProcessor.getTags(tokens);
		log.info("Tokens ({}): {}",tokens.length, Arrays.deepToString(tokens));
		log.info("Tags ({}): {}", tags.length, Arrays.deepToString(tags));
		
		return buildExercise(content, tokens, getGapPositionsFromTags(tags));
	}
	
	private Exercise buildExercise(Content content, String[] tokens, List<Integer> gapPos) {
		log.info("Building exercise...");
		
		Exercise ex = new Exercise();
		ex.setContent(content)
		.getQuestion()
		.setWords(Arrays.asList(tokens))
		.setGapPositions(gapPos);
		
		//replaces question's word with placeholder at the same time that it adds it to answer's word list
		ArrayList<String> answerWords = new ArrayList<>();
		gapPos.forEach(e -> 
			answerWords.add(ex.getQuestion().getWords().set(e, OPEN_CLOZE_PLACEHOLDER))
		);
		
		ex.getAnswer()
		.setWords(answerWords);
		
		return ex;
	}
	
	private List<Integer> getGapPositionsFromTags(String[] tags) {
		
		int maxAllowedGaps = (int) Math.ceil(GAP_QUESTION_RATIO * tags.length);
		
		// Get the idx of all suitable words, e.g whose tag-flag are set "true" in enum
		List<Integer> gapCandidates = getCandidateWordIndices(tags);
		
		// filter candidates for gaps closer than defined boundary and
		// prune until amount below threshold
		gapBoundaryFilter(gapCandidates);
		log.info("Maximum allowed gaps for {} words are {}", tags.length, maxAllowedGaps);
		pruneGapCandidateAmount(gapCandidates, maxAllowedGaps);
		
		// if empty after filtering and pruning, not usable.
		if (gapCandidates.isEmpty()) {
			log.warn("Suitable candidate word for cloze generation not found...");
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					OPEN_CLOZE_GENERATION_ERROR_MESSAGE);
		} else {
			log.info("Final gap selection: {}", gapCandidates.toString());
			return gapCandidates;
		}
	}
	
	private List<Integer> gapBoundaryFilter(List<Integer> gapCandidates) {
		log.info("Filtering according to gap boundary threshold of {} positions (inclusive)", GAP_BOUNDARY);
		boolean boundaryBreach = false;

		// if less than 2 elements, no pair to compare. Just return.
		if (gapCandidates.size() < 2) {
			log.info("Less than 2 items in gap candidate list. Returning");
			return gapCandidates;
		} else {
			// Check consecutive pairs if they are within boundary. remove if so.
			for (int i = 0; i < gapCandidates.size() - 1; i++) {
				int currentPos = gapCandidates.get(i);
				int nextPos = gapCandidates.get(i + 1);
				int proximity = nextPos - currentPos;
				if (proximity <= GAP_BOUNDARY) {
					log.info("Gaps too close to each other. Removing one...");
					boundaryBreach = true;
					gapCandidates.remove(i + new Random().nextInt(2));
					break;
				}
			}
			if (boundaryBreach) {
				log.info("Rechecking for proximity.");
				gapBoundaryFilter(gapCandidates);
			} else {
				log.info("All good, no gaps too close.");
				log.info("Candidates after filter: {}", gapCandidates.toString());
			}
			return gapCandidates;
		}
	}
	
	/**
	 * Returns List with equal or less gaps than the maximum allowed. 
	 */
	private List<Integer> pruneGapCandidateAmount(List<Integer> gapCandidates, int maxAllowedGaps){
		if(gapCandidates.size() <= maxAllowedGaps) {
			log.info("Compliant with gap quantity, moving on.");
			return gapCandidates;
		}else {
			// remove one at random and recurse until compliant.
			log.info("Removing one item...");
			gapCandidates.remove(new Random().nextInt(gapCandidates.size()));
			return pruneGapCandidateAmount(gapCandidates, maxAllowedGaps);
		}
	}
	
	/**
	 * returns a list of indices filtered from the list of speech tags provided,
	 * according to whether they are enabled for open cloze generation
	 * in the speech-tag Enum.
	 * @param tags
	 * @return
	 */
	private List<Integer> getCandidateWordIndices(String[] tags){
		List<Integer> gapCandidates = new ArrayList<>();
		
		for (int i = 0; i < tags.length; i++) {
			try {
				if(PartOfSpeechTags.isOpenCloze(tags[i])){
					gapCandidates.add(i);
				}
			} catch (IllegalArgumentException e) {
				log.warn("Warning. Tag {} is not in the parts of speech enum", tags[i]);
			}
		}
		if(gapCandidates.isEmpty()) {
			log.warn("Sentence has no suitable candidate words corresponding to tags for cloze generation.");
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR, 
					OPEN_CLOZE_GENERATION_ERROR_MESSAGE);
		}else {
			log.info("Candidate word indices for gap selection are {}", gapCandidates.toString());		
			return gapCandidates;
		}
	}
}
