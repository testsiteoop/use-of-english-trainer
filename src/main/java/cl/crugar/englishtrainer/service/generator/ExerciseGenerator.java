package cl.crugar.englishtrainer.service.generator;

import cl.crugar.englishtrainer.model.Content;
import cl.crugar.englishtrainer.model.Exercise;

public interface ExerciseGenerator {
	Exercise generate(Content content);
}
