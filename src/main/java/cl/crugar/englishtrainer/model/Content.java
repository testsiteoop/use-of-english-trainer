package cl.crugar.englishtrainer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Content {
	private Integer index;
	private String text;
	
	public Content(Integer index, String text) {
		this.index = index;
		this.text = text;
	}
}
