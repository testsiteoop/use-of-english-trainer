package cl.crugar.englishtrainer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Result {
	String given;
	String expected;
	boolean isCorrect;
}
