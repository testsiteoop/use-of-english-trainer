package cl.crugar.englishtrainer.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Exercise {
	
	public Exercise() {
		question = new Question();
		question.setExerciseKey(this.hashCode());
		answer = new Answer();
		answer.setExerciseKey(this.hashCode());
	}
	
	private Content content;
	private Question question;
	private Answer answer;
	
}
