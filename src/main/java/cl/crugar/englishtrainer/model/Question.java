package cl.crugar.englishtrainer.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Question {
	
	private int exerciseKey;
	private List<String> words;
	private List<Integer> gapPositions;

}
