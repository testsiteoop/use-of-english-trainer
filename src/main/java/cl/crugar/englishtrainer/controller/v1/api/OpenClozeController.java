package cl.crugar.englishtrainer.controller.v1.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.crugar.englishtrainer.model.Answer;
import cl.crugar.englishtrainer.model.Exercise;
import cl.crugar.englishtrainer.model.Question;
import cl.crugar.englishtrainer.model.Result;
import cl.crugar.englishtrainer.service.generator.ExerciseGenerator;
import cl.crugar.englishtrainer.service.provider.SentenceProvider;
import cl.crugar.englishtrainer.service.result.ResultAnalisis;
import cl.crugar.englishtrainer.session.store.ExerciseStorageHandler;
import cl.crugar.englishtrainer.session.store.ExerciseStore;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class OpenClozeController {
	
	private SentenceProvider sentenceProvider;
	private ExerciseGenerator openClozeGenerator;
	private ExerciseStorageHandler exerciseStore;
	private ResultAnalisis resultChecker;
	
	@Autowired
	public OpenClozeController(SentenceProvider sentenceProvider, ExerciseGenerator openClozeGenerator, 
			ExerciseStorageHandler exerciseStore, ResultAnalisis resultChecker) {
		this.sentenceProvider = sentenceProvider;
		this.openClozeGenerator = openClozeGenerator;
		this.exerciseStore = exerciseStore;
		this.resultChecker = resultChecker;
	}
	
	@GetMapping("/opencloze")
	public Question getOpenClozeSentence() {
		log.info("Generating random exercise...");
		Exercise exercise = openClozeGenerator.generate(sentenceProvider.getRandom());
		exerciseStore.put(exercise.hashCode(), exercise);
		return exercise.getQuestion();
	}
	
	@GetMapping("/opencloze/next")
	public Question getNextOpenClozeSentence(@RequestParam int key) {
		log.info("Generating next exercise for key {}", key);
		Exercise exercise = exerciseStore.get(key);
		Exercise nextExercise = openClozeGenerator.generate(sentenceProvider.getNext(exercise.getContent()));
		exerciseStore.put(nextExercise.hashCode(), nextExercise);
		return nextExercise.getQuestion();
	}
	
	@PostMapping("/opencloze")
	public List<Result> getResults(@RequestBody Answer givenAnswer) {
		Exercise exercise = exerciseStore.get(givenAnswer.getExerciseKey());
		return resultChecker.getResults(givenAnswer, exercise);
	}
	
	@Profile("dev")
	@GetMapping("/store")
	public Map<Integer, Exercise> getStore() {
		return ((ExerciseStore)exerciseStore).getStore();
	}
}
