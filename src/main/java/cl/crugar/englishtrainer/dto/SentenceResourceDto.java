package cl.crugar.englishtrainer.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SentenceResourceDto {
	private List<SentenceDto> data;
}
