package cl.crugar.englishtrainer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UseOfEnglishTrainerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UseOfEnglishTrainerApplication.class, args);
	}

}
