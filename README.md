# English-Trainer
Hi!
A simple proof of concept web application integrating various technologies.
You can check the Frontend source code over here: [Check the Source Code](https://bitbucket.org/testsiteoop/use-of-english-trainer/src/master/src/main/webapp/ "Follow me")

## Tech Stack

### Frontend
* ReactJS
* Bootstrap 4
* Axios

### Backend
* Java
* Spring Boot
* Apache OpenNLP